﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using ZaregMe.Model;

namespace ZaregMe.Migrations
{
    [DbContext(typeof(ZaregMeDb))]
    [Migration("20160928151139_Users")]
    partial class Users
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("ZaregMe.Model.User", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken()
                        .IsRequired()
                        .HasAnnotation("MaxLength", 50);

                    b.Property<DateTimeOffset>("Created")
                        .HasAnnotation("SqlServer:ColumnType", "datetimeoffset(0)");

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 100);

                    b.Property<bool>("IsAdmin");

                    b.Property<bool>("IsDisabled");

                    b.Property<bool>("IsEmailConfirmed");

                    b.Property<bool>("IsVerifiedManager");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 200);

                    b.Property<string>("NormalizedEmail")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 100);

                    b.Property<string>("NormalizedName")
                        .HasAnnotation("MaxLength", 200);

                    b.Property<string>("PasswordHash")
                        .HasAnnotation("MaxLength", 250);

                    b.Property<string>("SecurityStamp")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 50);

                    b.HasKey("Id");

                    b.HasIndex("Created");

                    b.HasIndex("NormalizedEmail")
                        .IsUnique();

                    b.ToTable("Users");
                });
        }
    }
}
