﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using ZaregMe.Model;

namespace ZaregMe.Migrations
{
    [DbContext(typeof(ZaregMeDb))]
    [Migration("20160928151238_Meetings")]
    partial class Meetings
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("ZaregMe.Model.Meeting", b =>
                {
                    b.Property<int>("Id");

                    b.Property<DateTimeOffset>("ArchiveTime")
                        .HasAnnotation("SqlServer:ColumnType", "datetimeoffset(0)");

                    b.Property<DateTimeOffset?>("CleanupTime")
                        .HasAnnotation("SqlServer:ColumnType", "datetimeoffset(0)");

                    b.Property<string>("CustomField1Info")
                        .HasAnnotation("MaxLength", 1000);

                    b.Property<string>("CustomField1Name")
                        .HasAnnotation("MaxLength", 100);

                    b.Property<string>("CustomField1Placeholder")
                        .HasAnnotation("MaxLength", 100);

                    b.Property<byte>("CustomField1Type");

                    b.Property<byte>("CustomField1Visibility");

                    b.Property<string>("CustomField2Info")
                        .HasAnnotation("MaxLength", 1000);

                    b.Property<string>("CustomField2Name")
                        .HasAnnotation("MaxLength", 100);

                    b.Property<string>("CustomField2Placeholder")
                        .HasAnnotation("MaxLength", 100);

                    b.Property<byte>("CustomField2Type");

                    b.Property<byte>("CustomField2Visibility");

                    b.Property<string>("CustomField3Info")
                        .HasAnnotation("MaxLength", 1000);

                    b.Property<string>("CustomField3Name")
                        .HasAnnotation("MaxLength", 100);

                    b.Property<string>("CustomField3Placeholder")
                        .HasAnnotation("MaxLength", 100);

                    b.Property<byte>("CustomField3Type");

                    b.Property<byte>("CustomField3Visibility");

                    b.Property<string>("CustomField4Info")
                        .HasAnnotation("MaxLength", 1000);

                    b.Property<string>("CustomField4Name")
                        .HasAnnotation("MaxLength", 100);

                    b.Property<string>("CustomField4Placeholder")
                        .HasAnnotation("MaxLength", 100);

                    b.Property<byte>("CustomField4Type");

                    b.Property<byte>("CustomField4Visibility");

                    b.Property<string>("CustomField5Info")
                        .HasAnnotation("MaxLength", 1000);

                    b.Property<string>("CustomField5Name")
                        .HasAnnotation("MaxLength", 100);

                    b.Property<string>("CustomField5Placeholder")
                        .HasAnnotation("MaxLength", 100);

                    b.Property<byte>("CustomField5Type");

                    b.Property<byte>("CustomField5Visibility");

                    b.Property<bool>("IsAcceptRequired");

                    b.Property<bool>("IsHidden");

                    b.Property<bool>("IsRegPaused");

                    b.Property<bool>("IsVerified");

                    b.Property<string>("LOCContact")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 100);

                    b.Property<string>("LOCName")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 250);

                    b.Property<string>("LargeLogoName")
                        .HasAnnotation("MaxLength", 100);

                    b.Property<string>("Location")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 100);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 100);

                    b.Property<byte>("NeedCoaches");

                    b.Property<byte>("NeedCustomField1");

                    b.Property<byte>("NeedCustomField2");

                    b.Property<byte>("NeedCustomField3");

                    b.Property<byte>("NeedCustomField4");

                    b.Property<byte>("NeedCustomField5");

                    b.Property<byte>("NeedEmail");

                    b.Property<byte>("NeedFatherName");

                    b.Property<byte>("NeedFullBirthDate");

                    b.Property<byte>("NeedHiddenFlag1");

                    b.Property<byte>("NeedLatinName");

                    b.Property<byte>("NeedMoscowDistrict");

                    b.Property<byte>("NeedPhone");

                    b.Property<byte>("NeedSportsCategory");

                    b.Property<byte>("NeedTeam");

                    b.Property<Guid>("OwnerUserId");

                    b.Property<int?>("ParticipantsLimit");

                    b.Property<DateTimeOffset>("ParticipantsListLastSendTime")
                        .HasAnnotation("SqlServer:ColumnType", "datetimeoffset(0)");

                    b.Property<string>("ParticipantsNote");

                    b.Property<int?>("ParticipantsQueueLimit");

                    b.Property<string>("ReferenceValueCoachByHimself")
                        .HasAnnotation("MaxLength", 100);

                    b.Property<string>("ReferenceValueTeamIndividually")
                        .HasAnnotation("MaxLength", 100);

                    b.Property<DateTimeOffset>("RegCloseTime")
                        .HasAnnotation("SqlServer:ColumnType", "datetimeoffset(0)");

                    b.Property<DateTimeOffset>("RegOpenTime")
                        .HasAnnotation("SqlServer:ColumnType", "datetimeoffset(0)");

                    b.Property<DateTimeOffset>("ShutdownTime")
                        .HasAnnotation("SqlServer:ColumnType", "datetimeoffset(0)");

                    b.Property<string>("SmallLogoName")
                        .HasAnnotation("MaxLength", 100);

                    b.Property<byte>("SportType");

                    b.Property<DateTime>("StartDate");

                    b.Property<byte>("StartListGenerationFormat");

                    b.Property<bool>("StartNumsAssignedOnAccept");

                    b.Property<int?>("SubdomainId");

                    b.Property<string>("TimeZoneId")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 250);

                    b.Property<TimeSpan>("TimeZoneOffset")
                        .HasAnnotation("SqlServer:ColumnType", "time(0)");

                    b.Property<byte>("TimingSystemType");

                    b.Property<string>("Url")
                        .HasAnnotation("MaxLength", 250);

                    b.Property<DateTimeOffset>("ValidateGeoBoundary")
                        .HasAnnotation("SqlServer:ColumnType", "datetimeoffset(0)");

                    b.Property<DateTimeOffset>("ValidateTeamsBoundary")
                        .HasAnnotation("SqlServer:ColumnType", "datetimeoffset(0)");

                    b.Property<string>("VerificationRequestMessage");

                    b.HasKey("Id");

                    b.HasIndex("OwnerUserId");

                    b.HasIndex("ShutdownTime");

                    b.ToTable("Meetings");
                });

            modelBuilder.Entity("ZaregMe.Model.User", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken()
                        .IsRequired()
                        .HasAnnotation("MaxLength", 50);

                    b.Property<DateTimeOffset>("Created")
                        .HasAnnotation("SqlServer:ColumnType", "datetimeoffset(0)");

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 100);

                    b.Property<bool>("IsAdmin");

                    b.Property<bool>("IsDisabled");

                    b.Property<bool>("IsEmailConfirmed");

                    b.Property<bool>("IsVerifiedManager");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 200);

                    b.Property<string>("NormalizedEmail")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 100);

                    b.Property<string>("NormalizedName")
                        .HasAnnotation("MaxLength", 200);

                    b.Property<string>("PasswordHash")
                        .HasAnnotation("MaxLength", 250);

                    b.Property<string>("SecurityStamp")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 50);

                    b.HasKey("Id");

                    b.HasIndex("Created");

                    b.HasIndex("NormalizedEmail")
                        .IsUnique();

                    b.ToTable("Users");
                });

            modelBuilder.Entity("ZaregMe.Model.Meeting", b =>
                {
                    b.HasOne("ZaregMe.Model.User", "OwnerUser")
                        .WithMany()
                        .HasForeignKey("OwnerUserId");
                });
        }
    }
}
