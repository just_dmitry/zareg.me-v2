﻿namespace ZaregMe.Migrations
{
    using System;
    using System.Collections.Generic;
    using Microsoft.EntityFrameworkCore.Migrations;

    public partial class Meetings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Meetings",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    ArchiveTime = table.Column<DateTimeOffset>(type: "datetimeoffset(0)", nullable: false),
                    CleanupTime = table.Column<DateTimeOffset>(type: "datetimeoffset(0)", nullable: true),
                    CustomField1Info = table.Column<string>(maxLength: 1000, nullable: true),
                    CustomField1Name = table.Column<string>(maxLength: 100, nullable: true),
                    CustomField1Placeholder = table.Column<string>(maxLength: 100, nullable: true),
                    CustomField1Type = table.Column<byte>(nullable: false),
                    CustomField1Visibility = table.Column<byte>(nullable: false),
                    CustomField2Info = table.Column<string>(maxLength: 1000, nullable: true),
                    CustomField2Name = table.Column<string>(maxLength: 100, nullable: true),
                    CustomField2Placeholder = table.Column<string>(maxLength: 100, nullable: true),
                    CustomField2Type = table.Column<byte>(nullable: false),
                    CustomField2Visibility = table.Column<byte>(nullable: false),
                    CustomField3Info = table.Column<string>(maxLength: 1000, nullable: true),
                    CustomField3Name = table.Column<string>(maxLength: 100, nullable: true),
                    CustomField3Placeholder = table.Column<string>(maxLength: 100, nullable: true),
                    CustomField3Type = table.Column<byte>(nullable: false),
                    CustomField3Visibility = table.Column<byte>(nullable: false),
                    CustomField4Info = table.Column<string>(maxLength: 1000, nullable: true),
                    CustomField4Name = table.Column<string>(maxLength: 100, nullable: true),
                    CustomField4Placeholder = table.Column<string>(maxLength: 100, nullable: true),
                    CustomField4Type = table.Column<byte>(nullable: false),
                    CustomField4Visibility = table.Column<byte>(nullable: false),
                    CustomField5Info = table.Column<string>(maxLength: 1000, nullable: true),
                    CustomField5Name = table.Column<string>(maxLength: 100, nullable: true),
                    CustomField5Placeholder = table.Column<string>(maxLength: 100, nullable: true),
                    CustomField5Type = table.Column<byte>(nullable: false),
                    CustomField5Visibility = table.Column<byte>(nullable: false),
                    IsAcceptRequired = table.Column<bool>(nullable: false),
                    IsHidden = table.Column<bool>(nullable: false),
                    IsRegPaused = table.Column<bool>(nullable: false),
                    IsVerified = table.Column<bool>(nullable: false),
                    LOCContact = table.Column<string>(maxLength: 100, nullable: false),
                    LOCName = table.Column<string>(maxLength: 250, nullable: false),
                    LargeLogoName = table.Column<string>(maxLength: 100, nullable: true),
                    Location = table.Column<string>(maxLength: 100, nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    NeedCoaches = table.Column<byte>(nullable: false),
                    NeedCustomField1 = table.Column<byte>(nullable: false),
                    NeedCustomField2 = table.Column<byte>(nullable: false),
                    NeedCustomField3 = table.Column<byte>(nullable: false),
                    NeedCustomField4 = table.Column<byte>(nullable: false),
                    NeedCustomField5 = table.Column<byte>(nullable: false),
                    NeedEmail = table.Column<byte>(nullable: false),
                    NeedFatherName = table.Column<byte>(nullable: false),
                    NeedFullBirthDate = table.Column<byte>(nullable: false),
                    NeedHiddenFlag1 = table.Column<byte>(nullable: false),
                    NeedLatinName = table.Column<byte>(nullable: false),
                    NeedMoscowDistrict = table.Column<byte>(nullable: false),
                    NeedPhone = table.Column<byte>(nullable: false),
                    NeedSportsCategory = table.Column<byte>(nullable: false),
                    NeedTeam = table.Column<byte>(nullable: false),
                    OwnerUserId = table.Column<Guid>(nullable: false),
                    ParticipantsLimit = table.Column<int>(nullable: true),
                    ParticipantsListLastSendTime = table.Column<DateTimeOffset>(type: "datetimeoffset(0)", nullable: false),
                    ParticipantsNote = table.Column<string>(nullable: true),
                    ParticipantsQueueLimit = table.Column<int>(nullable: true),
                    ReferenceValueCoachByHimself = table.Column<string>(maxLength: 100, nullable: true),
                    ReferenceValueTeamIndividually = table.Column<string>(maxLength: 100, nullable: true),
                    RegCloseTime = table.Column<DateTimeOffset>(type: "datetimeoffset(0)", nullable: false),
                    RegOpenTime = table.Column<DateTimeOffset>(type: "datetimeoffset(0)", nullable: false),
                    ShutdownTime = table.Column<DateTimeOffset>(type: "datetimeoffset(0)", nullable: false),
                    SmallLogoName = table.Column<string>(maxLength: 100, nullable: true),
                    SportType = table.Column<byte>(nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    StartListGenerationFormat = table.Column<byte>(nullable: false),
                    StartNumsAssignedOnAccept = table.Column<bool>(nullable: false),
                    SubdomainId = table.Column<int>(nullable: true),
                    TimeZoneId = table.Column<string>(maxLength: 250, nullable: false),
                    TimeZoneOffset = table.Column<TimeSpan>(type: "time(0)", nullable: false),
                    TimingSystemType = table.Column<byte>(nullable: false),
                    Url = table.Column<string>(maxLength: 250, nullable: true),
                    ValidateGeoBoundary = table.Column<DateTimeOffset>(type: "datetimeoffset(0)", nullable: false),
                    ValidateTeamsBoundary = table.Column<DateTimeOffset>(type: "datetimeoffset(0)", nullable: false),
                    VerificationRequestMessage = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Meetings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Meetings_Users_OwnerUserId",
                        column: x => x.OwnerUserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Meetings_OwnerUserId",
                table: "Meetings",
                column: "OwnerUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Meetings_ShutdownTime",
                table: "Meetings",
                column: "ShutdownTime");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Meetings");
        }
    }
}
