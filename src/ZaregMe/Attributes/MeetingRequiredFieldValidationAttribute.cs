﻿namespace ZaregMe.Attributes
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using Model;

    public class MeetingRequiredFieldValidationAttribute : RequiredAttribute
    {
        public MeetingRequiredFieldValidationAttribute(string field)
        {
            this.Field = field;
        }

        public string Field { get; set; }

        public override bool RequiresValidationContext
        {
            get { return true; }
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var m = validationContext.ObjectInstance as IMeetingFields;
            if (m == null)
            {
                return ValidationResult.Success;
            }

            var val = value as string;
            if (val == null)
            {
                return ValidationResult.Success;
            }

            var need = FieldNecessityEnum.Hidden;
            switch (Field)
            {
                case "NeedTeam":
                    need = m.NeedTeam;
                    break;
                case "NeedCoaches":
                    need = m.NeedCoaches;
                    break;
                default:
                    throw new Exception($"Unknown field: '{Field}'");
            }

            if (need == FieldNecessityEnum.Required)
            {
                if (string.IsNullOrWhiteSpace(val))
                {
                    return new ValidationResult(
                        FormatErrorMessage(validationContext.DisplayName),
                        new[] { validationContext.MemberName });
                }
            }

            return ValidationResult.Success;
        }
    }
}