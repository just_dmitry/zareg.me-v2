﻿namespace ZaregMe
{
    using System;
    using System.Globalization;
    using System.Text.Unicode;
    using Logging.ExceptionSender;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Routing;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Model;
    using RecurrentTasks;
    using Services;

    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json");

            // Workaround for https://github.com/aspnet/UserSecrets/issues/62
            if (builder.GetFileProvider().GetFileInfo("project.json").Exists)
            {
                builder.AddUserSecrets();
            }

            builder.AddEnvironmentVariables();

            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(Configuration);

            services.Configure<Microsoft.Extensions.WebEncoders.WebEncoderOptions>(
               o =>
               {
                   o.TextEncoderSettings = new System.Text.Encodings.Web.TextEncoderSettings();
                   o.TextEncoderSettings.AllowRanges(new[] { UnicodeRanges.BasicLatin, UnicodeRanges.Cyrillic });
               });

            var dbConnectionString = Configuration.GetValue<string>("DB:ConnectionString");
            services.AddDbContext<ZaregMeDb>(o => o.UseSqlServer(dbConnectionString));

            services.AddIdentity<User, UserRole>().AddUserStore<ZaregMeDb>();

            services.AddMvc(options =>
            {
                options.Filters.Add(new AutoValidateAntiforgeryTokenAttribute());
            });

            services.Configure<RouteOptions>(options =>
            {
                options.LowercaseUrls = true;
            });

            services.AddSingleton<ExceptionSenderTask, ExceptionSenderMailgunTask>();
            services.Configure<ExceptionSenderOptions>(Configuration.GetSection("ExceptionSender"));
            services.Configure<ExceptionSenderMailgunOptions>(Configuration.GetSection("ExceptionSender"));

            services.AddSingleton<MeetingCacheService>();

            services.Configure<DatabaseDefaultValues>(Configuration.GetSection("DatabaseDefaultValues"));
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddMemory(Configuration.GetSection("Logging"));

            app.UseStatusCodePages();
            app.UseDeveloperExceptionPage();
            app.UseExceptionSender();

            var staticFileOptions = new StaticFileOptions
            {
                OnPrepareResponse = (context) =>
                {
                    context.Context.Response.Headers.Add("Cache-Control", "public, max-age=15552000"); // 180 days
                }
            };
            app.UseStaticFiles(staticFileOptions);

            var ruCulture = new CultureInfo("ru-RU");
            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                DefaultRequestCulture = new Microsoft.AspNetCore.Localization.RequestCulture("ru-RU"),
                SupportedCultures = new[] { ruCulture },
                SupportedUICultures = new[] { ruCulture }
            });

            app.UseIdentity();

            app.UseMvc();

            var logger = app.ApplicationServices.GetRequiredService<ILogger<Startup>>();

            logger.LogDebug("Seeding database");
            var scopeFactory = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>();
            using (var scope = scopeFactory.CreateScope())
            {
                var db = scope.ServiceProvider.GetRequiredService<ZaregMeDb>();
                db.Database.Migrate();

                var options = scope.ServiceProvider.GetRequiredService<IOptions<DatabaseDefaultValues>>().Value;
                var normalizer = scope.ServiceProvider.GetRequiredService<ILookupNormalizer>();
                db.SeedAsync(options, normalizer).Wait();
            }

            logger.LogDebug("Starting all tasks");

            var exceptionSenderTask = app.ApplicationServices.GetRequiredService<ExceptionSenderTask>();
            if (!env.IsDevelopment())
            {
                exceptionSenderTask.Start();
            }

            app.ApplicationServices.GetRequiredService<MeetingCacheService>()
                .CatchExceptions(exceptionSenderTask)
                .Start(TimeSpan.FromSeconds(3));

            logger.LogInformation("Configure() done.");
        }
    }
}
