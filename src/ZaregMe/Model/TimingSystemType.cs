﻿namespace ZaregMe.Model
{
    public enum TimingSystemType : byte
    {
        DefaultNone,
        Muravey1993,
        Parsec2003
    }
}