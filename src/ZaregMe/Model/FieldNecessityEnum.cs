﻿namespace ZaregMe.Model
{
    public enum FieldNecessityEnum : byte
    {
        Hidden = 0,
        Optional = 1,
        Required = 2
    }
}