﻿namespace ZaregMe.Model
{
    public enum CustomFieldVisibilityEnum : byte
    {
        Participant,
        ParticipantEditAnyView,
        ManagerEditParticipantView,
        ManagerEditAnyView
    }
}