﻿namespace ZaregMe.Model
{
    using System;

    public interface IMeetingGeneralInfo
    {
        string Name { get; set; }

        DateTime StartDate { get; set; }

        string Location { get; set; }

        string Url { get; set; }

        string LOCName { get; set; }

        string LOCContact { get; set; }

        string TimeZoneId { get; set; }

        TimeSpan TimeZoneOffset { get; set; }

        bool IsHidden { get; set; }

        Guid OwnerUserId { get; set; }

        bool IsVerified { get; set; }

        string VerificationRequestMessage { get; set; }

        SportTypeEnum SportType { get; set; }

        int? SubdomainId { get; set; }
    }
}
