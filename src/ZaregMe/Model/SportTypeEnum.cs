﻿namespace ZaregMe.Model
{
    public enum SportTypeEnum : byte
    {
        Unknown = 0,
        Athletics_Running = 1,
        Cycling = 2
    }
}