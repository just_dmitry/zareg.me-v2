﻿namespace ZaregMe.Model
{
    using System;

    public interface IMeetingRegDetails
    {
        DateTimeOffset RegOpenTime { get; set; }

        DateTimeOffset RegCloseTime { get; set; }

        bool IsRegPaused { get; set; }

        string ParticipantsNote { get; set; }

        bool IsAcceptRequired { get; set; }

        bool StartNumsAssignedOnAccept { get; set; }

        int? ParticipantsLimit { get; set; }

        int? ParticipantsQueueLimit { get; set; }
    }
}
