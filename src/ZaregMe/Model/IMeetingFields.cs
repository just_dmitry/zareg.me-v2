﻿namespace ZaregMe.Model
{
    public interface IMeetingFields
    {
        FieldNecessityEnum NeedFatherName { get; set; }

        FieldNecessityEnum NeedLatinName { get; set; }

        FieldNecessityEnum NeedFullBirthDate { get; set; }

        FieldNecessityEnum NeedTeam { get; set; }

        FieldNecessityEnum NeedSportsCategory { get; set; }

        FieldNecessityEnum NeedMoscowDistrict { get; set; }

        FieldNecessityEnum NeedCoaches { get; set; }

        FieldNecessityEnum NeedPhone { get; set; }

        FieldNecessityEnum NeedEmail { get; set; }

        string ReferenceValueTeamIndividually { get; set; }

        string ReferenceValueCoachByHimself { get; set; }

        /* Custom field 1 */

        FieldNecessityEnum NeedCustomField1 { get; set; }

        string CustomField1Name { get; set; }

        string CustomField1Placeholder { get; set; }

        string CustomField1Info { get; set; }

        CustomFieldTypeEnum CustomField1Type { get; set; }

        CustomFieldVisibilityEnum CustomField1Visibility { get; set; }

        /* Custom field 2 */

        FieldNecessityEnum NeedCustomField2 { get; set; }

        string CustomField2Name { get; set; }

        string CustomField2Placeholder { get; set; }

        string CustomField2Info { get; set; }

        CustomFieldTypeEnum CustomField2Type { get; set; }

        CustomFieldVisibilityEnum CustomField2Visibility { get; set; }

        /* Custom field 3 */

        FieldNecessityEnum NeedCustomField3 { get; set; }

        string CustomField3Name { get; set; }

        string CustomField3Placeholder { get; set; }

        string CustomField3Info { get; set; }

        CustomFieldTypeEnum CustomField3Type { get; set; }

        CustomFieldVisibilityEnum CustomField3Visibility { get; set; }

        /* Custom field 4 */

        FieldNecessityEnum NeedCustomField4 { get; set; }

        string CustomField4Name { get; set; }

        string CustomField4Placeholder { get; set; }

        string CustomField4Info { get; set; }

        CustomFieldTypeEnum CustomField4Type { get; set; }

        CustomFieldVisibilityEnum CustomField4Visibility { get; set; }

        /* Custom field 5 */

        FieldNecessityEnum NeedCustomField5 { get; set; }

        string CustomField5Name { get; set; }

        string CustomField5Placeholder { get; set; }

        string CustomField5Info { get; set; }

        CustomFieldTypeEnum CustomField5Type { get; set; }

        CustomFieldVisibilityEnum CustomField5Visibility { get; set; }
    }
}
