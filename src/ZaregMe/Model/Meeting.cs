namespace ZaregMe.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata;
    using ZaregMe.Attributes;

    public partial class Meeting : IMeetingFields, IMeetingGeneralInfo, IMeetingIntegrations, IMeetingRegDetails, IMeetingStatistics
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Required]
        public int Id { get; set; }

        #region IMeetingGeneralInfo

        [MaxLength(100)]
        [Required]
        public string Name { get; set; }

        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d}")]
        public DateTime StartDate { get; set; }

        [MaxLength(100)]
        [Required]
        public string Location { get; set; }

        [MaxLength(250)]
        public string Url { get; set; }

        [Required]
        [MaxLength(250)]
        public string LOCName { get; set; }

        [Required]
        [MaxLength(100)]
        public string LOCContact { get; set; }

        [Required]
        [MaxLength(250)]
        public string TimeZoneId { get; set; }

        [Required]
        public TimeSpan TimeZoneOffset { get; set; }

        [Required]
        public bool IsHidden { get; set; }

        [Required]
        public Guid OwnerUserId { get; set; }

        public bool IsVerified { get; set; }

        [MaxLength]
        public string VerificationRequestMessage { get; set; }

        [Required]
        public SportTypeEnum SportType { get; set; }

        public int? SubdomainId { get; set; }

        #endregion

        [ForeignKey("OwnerUserId")]
        public virtual User OwnerUser { get; set; }

        ////[ForeignKey("SubdomainId")]
        ////public virtual Subdomain Subdomain { get; set; }

        #region IMeetingRegDetails

        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true)]
        public DateTimeOffset RegOpenTime { get; set; }

        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true)]
        public DateTimeOffset RegCloseTime { get; set; }

        [Required]
        public bool IsRegPaused { get; set; }

        [MaxLength]
        public string ParticipantsNote { get; set; }

        [Required]
        public bool IsAcceptRequired { get; set; }

        [Required]
        public bool StartNumsAssignedOnAccept { get; set; }

        [Range(0, 9999)]
        public int? ParticipantsLimit { get; set; }

        [Range(0, 9999)]
        public int? ParticipantsQueueLimit { get; set; }

        #endregion

        #region IMeetingFields

        [Required]
        public FieldNecessityEnum NeedFatherName { get; set; }

        [Required]
        public FieldNecessityEnum NeedLatinName { get; set; }

        [Required]
        public FieldNecessityEnum NeedFullBirthDate { get; set; }

        [Required]
        public FieldNecessityEnum NeedTeam { get; set; }

        [Required]
        public FieldNecessityEnum NeedSportsCategory { get; set; }

        [Required]
        public FieldNecessityEnum NeedMoscowDistrict { get; set; }

        [Required]
        public FieldNecessityEnum NeedCoaches { get; set; }

        [Required]
        public FieldNecessityEnum NeedPhone { get; set; }

        [Required]
        public FieldNecessityEnum NeedEmail { get; set; }

        [Required]
        public FieldNecessityEnum NeedHiddenFlag1 { get; set; }

        [MaxLength(100)]
        [MeetingRequiredFieldValidation(nameof(NeedTeam))]
        public string ReferenceValueTeamIndividually { get; set; }

        [MaxLength(100)]
        [MeetingRequiredFieldValidation(nameof(NeedCoaches))]
        public string ReferenceValueCoachByHimself { get; set; }

        /* Custom field 1 */

        [Required]
        public FieldNecessityEnum NeedCustomField1 { get; set; }

        [MaxLength(100)]
        public string CustomField1Name { get; set; }

        [MaxLength(100)]
        public string CustomField1Placeholder { get; set; }

        [MaxLength(1000)]
        public string CustomField1Info { get; set; }

        [Required]
        public CustomFieldTypeEnum CustomField1Type { get; set; }

        [Required]
        public CustomFieldVisibilityEnum CustomField1Visibility { get; set; }

        /* Custom field 3 */

        [Required]
        public FieldNecessityEnum NeedCustomField2 { get; set; }

        [MaxLength(100)]
        public string CustomField2Name { get; set; }

        [MaxLength(100)]
        public string CustomField2Placeholder { get; set; }

        [MaxLength(1000)]
        public string CustomField2Info { get; set; }

        [Required]
        public CustomFieldTypeEnum CustomField2Type { get; set; }

        [Required]
        public CustomFieldVisibilityEnum CustomField2Visibility { get; set; }

        /* Custom field 3 */

        [Required]
        public FieldNecessityEnum NeedCustomField3 { get; set; }

        [MaxLength(100)]
        public string CustomField3Name { get; set; }

        [MaxLength(100)]
        public string CustomField3Placeholder { get; set; }

        [MaxLength(1000)]
        public string CustomField3Info { get; set; }

        [Required]
        public CustomFieldTypeEnum CustomField3Type { get; set; }

        [Required]
        public CustomFieldVisibilityEnum CustomField3Visibility { get; set; }

        /* Custom field 4 */

        [Required]
        public FieldNecessityEnum NeedCustomField4 { get; set; }

        [MaxLength(100)]
        public string CustomField4Name { get; set; }

        [MaxLength(100)]
        public string CustomField4Placeholder { get; set; }

        [MaxLength(1000)]
        public string CustomField4Info { get; set; }

        [Required]
        public CustomFieldTypeEnum CustomField4Type { get; set; }

        [Required]
        public CustomFieldVisibilityEnum CustomField4Visibility { get; set; }

        /* Custom field 5 */

        [Required]
        public FieldNecessityEnum NeedCustomField5 { get; set; }

        [MaxLength(100)]
        public string CustomField5Name { get; set; }

        [MaxLength(100)]
        public string CustomField5Placeholder { get; set; }

        [MaxLength(1000)]
        public string CustomField5Info { get; set; }

        [Required]
        public CustomFieldTypeEnum CustomField5Type { get; set; }

        [Required]
        public CustomFieldVisibilityEnum CustomField5Visibility { get; set; }

        #endregion

        #region IMeetingIntegrations

        [Required]
        public StartListGenerationFormatEnum StartListGenerationFormat { get; set; }

        [Required]
        public TimingSystemType TimingSystemType { get; set; }

        #endregion

        #region IMeetingStatistics

        [Required]
        public DateTimeOffset ShutdownTime { get; set; }

        [Required]
        public DateTimeOffset ArchiveTime { get; set; }

        [Required]
        public DateTimeOffset ParticipantsListLastSendTime { get; set; }

        [Required]
        public DateTimeOffset ValidateTeamsBoundary { get; set; }

        [Required]
        public DateTimeOffset ValidateGeoBoundary { get; set; }

        public DateTimeOffset? CleanupTime { get; set; }

        #endregion

        [MaxLength(100)]
        public string SmallLogoName { get; set; }

        [MaxLength(100)]
        public string LargeLogoName { get; set; }

        public static void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Meeting>(t =>
            {
                // ���������� ������� ������ ��������, ��������������� �� RequiredAttribute
                t.Property(p => p.ReferenceValueTeamIndividually).IsRequired(false);
                t.Property(p => p.ReferenceValueCoachByHimself).IsRequired(false);

                // �������� �������� ����-�������
                t.Property(p => p.TimeZoneOffset).TimeZeroPrecision();
                t.Property(p => p.RegOpenTime).DateTimeOffsetZeroPrecision();
                t.Property(p => p.RegCloseTime).DateTimeOffsetZeroPrecision();
                t.Property(p => p.ShutdownTime).DateTimeOffsetZeroPrecision();
                t.Property(p => p.ArchiveTime).DateTimeOffsetZeroPrecision();
                t.Property(p => p.ParticipantsListLastSendTime).DateTimeOffsetZeroPrecision();
                t.Property(p => p.ValidateTeamsBoundary).DateTimeOffsetZeroPrecision();
                t.Property(p => p.ValidateGeoBoundary).DateTimeOffsetZeroPrecision();
                t.Property(p => p.CleanupTime).DateTimeOffsetZeroPrecision();

                t.HasIndex(p => p.ShutdownTime);
                t.HasOne(p => p.OwnerUser).WithMany().OnDelete(DeleteBehavior.Restrict);
            });
        }
    }
}
