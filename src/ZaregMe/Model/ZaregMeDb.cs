﻿namespace ZaregMe.Model
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.EntityFrameworkCore;

    public partial class ZaregMeDb : DbContext
    {
        public static readonly Guid AutocorrectUserId = new Guid("F63E5037-DAC8-4907-8C55-269244DB3D34");
        public static readonly Guid BackgroundTaskUserId = new Guid("74A69C2F-3D51-4BCF-A2F9-1CC49326EA4E");

        public ZaregMeDb(DbContextOptions<ZaregMeDb> options)
            : base(options)
        {
            // Nothing
        }

        public DbSet<User> Users { get; set; }

        public DbSet<Meeting> Meetings { get; set; }

        public async Task SeedAsync(DatabaseDefaultValues defaultValues, ILookupNormalizer normalizer)
        {
            // will save later
            AutoSaveChanges = false;

            // 1. Check admin
            var normalizedAdminEmail = normalizer.Normalize(defaultValues.AdminEmail);
            var admin = await Users.SingleOrDefaultAsync(x => x.NormalizedEmail == normalizedAdminEmail);
            if (admin == null)
            {
                admin = new User
                {
                    Id = Guid.NewGuid(),
                    Name = defaultValues.AdminEmail,
                    NormalizedName = normalizedAdminEmail,
                    Email = defaultValues.AdminEmail,
                    NormalizedEmail = normalizedAdminEmail,

                    IsEmailConfirmed = true,
                    ConcurrencyStamp = Guid.NewGuid().ToString(),
                    SecurityStamp = Guid.NewGuid().ToString(),
                    Created = DateTimeOffset.Now
                };
                Users.Add(admin);
            }

            if (!admin.IsAdmin)
            {
                admin.IsAdmin = true;
                admin.ConcurrencyStamp = Guid.NewGuid().ToString();
                admin.SecurityStamp = Guid.NewGuid().ToString();
            }

            if (admin.IsDisabled)
            {
                admin.IsDisabled = false;
                admin.ConcurrencyStamp = Guid.NewGuid().ToString();
                admin.SecurityStamp = Guid.NewGuid().ToString();
            }

            // 2. Check Autocorrect user
            var autocorrect = await Users.SingleOrDefaultAsync(x => x.Id == AutocorrectUserId);
            if (autocorrect == null)
            {
                autocorrect = new User
                {
                    Id = AutocorrectUserId,
                    Name = defaultValues.AutocorrectUserName,
                    NormalizedName = normalizer.Normalize(defaultValues.AutocorrectUserName),
                    Email = AutocorrectUserId.ToString(),
                    NormalizedEmail = normalizer.Normalize(AutocorrectUserId.ToString()),

                    IsEmailConfirmed = false,
                    ConcurrencyStamp = Guid.NewGuid().ToString(),
                    SecurityStamp = Guid.NewGuid().ToString(),
                    Created = DateTimeOffset.Now
                };
                Users.Add(autocorrect);
            }

            // 3. Check BackgroundTask user
            var backgroundTask = await Users.SingleOrDefaultAsync(x => x.Id == BackgroundTaskUserId);
            if (backgroundTask == null)
            {
                backgroundTask = new User
                {
                    Id = BackgroundTaskUserId,
                    Name = defaultValues.BackgroundTaskUserName,
                    NormalizedName = normalizer.Normalize(defaultValues.BackgroundTaskUserName),
                    Email = BackgroundTaskUserId.ToString(),
                    NormalizedEmail = normalizer.Normalize(BackgroundTaskUserId.ToString()),

                    IsEmailConfirmed = false,
                    ConcurrencyStamp = Guid.NewGuid().ToString(),
                    SecurityStamp = Guid.NewGuid().ToString(),
                    Created = DateTimeOffset.Now
                };
                Users.Add(backgroundTask);
            }

            // save all
            await SaveChangesAsync();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            User.OnModelCreating(modelBuilder);
            Meeting.OnModelCreating(modelBuilder);
        }
    }
}
