﻿namespace ZaregMe.Model
{
    using System;

    public interface IMeetingStatistics
    {
        DateTimeOffset ShutdownTime { get; set; }

        DateTimeOffset ArchiveTime { get; set; }

        DateTimeOffset ParticipantsListLastSendTime { get; set; }

        DateTimeOffset ValidateTeamsBoundary { get; set; }

        DateTimeOffset ValidateGeoBoundary { get; set; }

        DateTimeOffset? CleanupTime { get; set; }
    }
}
