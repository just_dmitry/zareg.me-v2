﻿namespace ZaregMe.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Microsoft.EntityFrameworkCore;

    public class User
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(200)]
        public string Name { get; set; }

        [MaxLength(200)]
        public string NormalizedName { get; set; }

        [Required]
        [MaxLength(100)]
        public string Email { get; set; }

        [Required]
        [MaxLength(100)]
        public string NormalizedEmail { get; set; }

        [MaxLength(250)]
        public string PasswordHash { get; set; }

        [Required]
        [MaxLength(50)]
        public string SecurityStamp { get; set; }

        [Required]
        [MaxLength(50)]
        [ConcurrencyCheck]
        public string ConcurrencyStamp { get; set; }

        [Required]
        public DateTimeOffset Created { get; set; }

        [Required]
        public bool IsEmailConfirmed { get; set; }

        [Required]
        public bool IsAdmin { get; set; }

        [Required]
        public bool IsVerifiedManager { get; set; }

        [Required]
        public bool IsDisabled { get; set; }

        public static void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>(t =>
            {
                t.Property(p => p.Created).DateTimeOffsetZeroPrecision();
                t.HasIndex(p => p.NormalizedEmail).IsUnique();
                t.HasIndex(p => p.Created);
            });
        }
    }
}
