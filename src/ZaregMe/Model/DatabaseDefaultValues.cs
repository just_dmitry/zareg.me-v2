﻿namespace ZaregMe.Model
{
    using System;

    public class DatabaseDefaultValues
    {
        public string AdminEmail { get; set; }

        public string AutocorrectUserName { get; set; }

        public string BackgroundTaskUserName { get; set; }
    }
}
