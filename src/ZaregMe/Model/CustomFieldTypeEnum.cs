﻿namespace ZaregMe.Model
{
    public enum CustomFieldTypeEnum : byte
    {
        String,
        StringLarge,
        StringMultiline,
        Numeric
    }
}