﻿namespace ZaregMe.Model
{
    public interface IMeetingIntegrations
    {
        StartListGenerationFormatEnum StartListGenerationFormat { get; set; }

        TimingSystemType TimingSystemType { get; set; }
    }
}
