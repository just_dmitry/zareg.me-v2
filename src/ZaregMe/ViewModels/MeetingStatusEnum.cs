﻿namespace ZaregMe.ViewModels
{
    public enum MeetingStatusEnum : byte
    {
        Refreshing,

        RegNotStarted,
        RegOpen,
        RegOpenPaused,
        RegOpenNearClose,
        RegOpenNearLimit,
        RegOpenToQueueOnly,
        RegOpenSuspendedQueueFull,
        RegClosed,
        Shutdown,
        Archive,
        NotVerified
    }
}