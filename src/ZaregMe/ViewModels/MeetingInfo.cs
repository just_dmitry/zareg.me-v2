﻿namespace ZaregMe.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using BootstrapMvc;
    using Model;
    using Resources;

    public class MeetingInfo
    {
        private int unaccountedParticipantsValue;

        public MeetingInfo()
        {
            StatusText = Messages.Meeting_Status_Refreshing;
            StatusTextShort = Messages.Meeting_Status_Refreshing_Short;
            Status = MeetingStatusEnum.Refreshing;
            LabelBadgeType = LabelType.DefaultGray;
            AlertType = AlertType.InfoCyan;
            PanelType = PanelType.DefaultGray;
        }

        /* just for cache */

        public int Id { get; set; }

        public DateTime StartDate { get; set; }

        public string Name { get; set; }

        public string Location { get; set; }

        public string SmallLogoName { get; set; }

        public bool IsHiddenFromStartPage { get; set; }

        public bool IsVerified { get; set; }

        public SportTypeEnum SportType { get; set; }

        public int? SubdomainId { get; set; }

        public TimeSpan TimeZoneOffset { get; set; }

        public Guid OwnerUserId { get; set; }

        /* display information  */

        public MeetingStatusEnum Status { get; set; }

        public string StatusText { get; set; }

        public string StatusTextShort { get; set; }

        public AlertType AlertType { get; set; }

        public LabelType LabelBadgeType { get; set; }

        public PanelType PanelType { get; set; }

        /* additional data */

        public DateTimeOffset RegOpenTime { get; set; }

        public DateTimeOffset RegCloseTime { get; set; }

        public DateTimeOffset ShutdownTime { get; set; }

        public DateTimeOffset ArchiveTime { get; set; }

        public bool IsRegPaused { get; set; }

        public int? NotInQueueLimit { get; set; }

        public Dictionary<Guid, int?> NotInQueueLimitByDistances { get; set; }

        public int? QueueLimit { get; set; }

        public Guid[] FriendUserIds { get; set; }

        public bool UsesPayments { get; set; }

        public int ParticipantsNotInQueue { get; set; }

        public int ParticipantsInQueue { get; set; }

        public int ParticipantsNotAccepted { get; set; }

        public IDictionary<Guid, int> ParticipantsNowInQueueByDistances { get; set; }

        public int UnaccountedParticipants
        {
            get { return unaccountedParticipantsValue; }
        }

        public void CopyFrom(Meeting m)
        {
            Id = m.Id;
            StartDate = m.StartDate;
            Name = m.Name;
            Location = m.Location;
            SmallLogoName = m.SmallLogoName;
            SportType = m.SportType;
            SubdomainId = m.SubdomainId;
            TimeZoneOffset = m.TimeZoneOffset;
            IsHiddenFromStartPage = m.IsHidden;
            IsVerified = m.IsVerified;
            NotInQueueLimit = m.ParticipantsLimit;
            ////if (m.Distances != null)
            ////{
            ////    NotInQueueLimitByDistances = m.Distances.ToDictionary(x => x.DistanceId, x => x.ParticipantsLimit);
            ////}
            QueueLimit = m.ParticipantsQueueLimit;

            RegOpenTime = m.RegOpenTime;
            RegCloseTime = m.RegCloseTime;
            IsRegPaused = m.IsRegPaused;
            ShutdownTime = m.ShutdownTime;
            ArchiveTime = m.ArchiveTime;

            OwnerUserId = m.OwnerUserId;

            FriendUserIds = new Guid[] { }; // m.MeetingFriends.Select(mf => mf.UserId).ToArray();

            UsesPayments = false; // m.MeetingPaymentRules.Any() || m.MeetingPaymentMethods.Any();
        }

        public void RefreshStatus()
        {
            var now = DateTimeOffset.Now;

            if (now > ArchiveTime)
            {
                StatusText = Messages.Meeting_Status_MeetingArchived;
                StatusTextShort = StatusText;
                Status = MeetingStatusEnum.Archive;
                LabelBadgeType = LabelType.DangerRed;
                AlertType = AlertType.DangerRed;
                PanelType = PanelType.DangerRed;
                return;
            }

            if (!IsVerified)
            {
                StatusText = Messages.Meeting_Status_MeetingNotVerified;
                StatusTextShort = StatusText;
                Status = MeetingStatusEnum.NotVerified;
                LabelBadgeType = LabelType.DangerRed;
                AlertType = AlertType.DangerRed;
                PanelType = PanelType.DangerRed;
                return;
            }

            if (now > ShutdownTime)
            {
                StatusText = Messages.Meeting_Status_MeetingClosed;
                StatusTextShort = StatusText;
                Status = MeetingStatusEnum.Shutdown;
                LabelBadgeType = LabelType.DangerRed;
                AlertType = AlertType.DangerRed;
                PanelType = PanelType.DangerRed;
                return;
            }

            if (now < RegOpenTime)
            {
                var leftToStart = RegOpenTime - now;
                if (leftToStart.TotalDays > 7)
                {
                    StatusText = string.Format(Messages.Meeting_Status_NotOpen_OpenAt, RegOpenTime.ToString("d MMMM"));
                }
                else
                {
                    StatusText = string.Format(Messages.Meeting_Status_NotOpen_OpenIn, leftToStart.ToHumanInterval());
                }

                StatusTextShort = StatusText;
                Status = MeetingStatusEnum.RegNotStarted;
                LabelBadgeType = LabelType.InfoCyan;
                AlertType = AlertType.InfoCyan;
                PanelType = PanelType.InfoCyan;
                return;
            }

            if (now > RegCloseTime)
            {
                StatusText = Messages.Meeting_Status_Closed;
                StatusTextShort = StatusText;
                Status = MeetingStatusEnum.RegClosed;
                LabelBadgeType = LabelType.DangerRed;
                AlertType = AlertType.DangerRed;
                PanelType = PanelType.DangerRed;
                return;
            }

            if (IsRegPaused)
            {
                StatusText = Messages.Meeting_Status_Paused;
                StatusTextShort = StatusText;
                Status = MeetingStatusEnum.RegOpenPaused;
                LabelBadgeType = LabelType.WarningOrange;
                AlertType = AlertType.WarningOrange;
                PanelType = PanelType.WarningOrange;
                return;
            }

            if (NotInQueueLimit.HasValue)
            {
                if (ParticipantsNotInQueue >= NotInQueueLimit.Value)
                {
                    if (ParticipantsInQueue >= (QueueLimit ?? 0))
                    {
                        Status = MeetingStatusEnum.RegOpenSuspendedQueueFull;
                        StatusText = Messages.Meeting_Status_OpenSuspendedQueueFull;
                        StatusTextShort = Messages.Meeting_Status_OpenSuspendedQueueFull_Short;
                        LabelBadgeType = LabelType.DangerRed;
                        AlertType = AlertType.DangerRed;
                        PanelType = PanelType.DangerRed;
                    }
                    else
                    {
                        Status = MeetingStatusEnum.RegOpenToQueueOnly;
                        StatusText = Messages.Meeting_Status_OpenToQueueOnly;
                        StatusTextShort = Messages.Meeting_Status_OpenToQueueOnly;
                        LabelBadgeType = LabelType.WarningOrange;
                        AlertType = AlertType.WarningOrange;
                        PanelType = PanelType.WarningOrange;
                    }

                    return;
                }
            }

            Status = MeetingStatusEnum.RegOpen;
            StatusText = Messages.Meeting_Status_Open;
            StatusTextShort = StatusText;
            LabelBadgeType = LabelType.SuccessGreen;
            AlertType = AlertType.SuccessGreen;
            PanelType = PanelType.SuccessGreen;

            if (NotInQueueLimit.HasValue && ((ParticipantsNotInQueue + ParticipantsInQueue) / (double)NotInQueueLimit.Value) > 0.95)
            {
                Status = MeetingStatusEnum.RegOpenNearLimit;
                StatusText += (NotInQueueLimit.Value - ParticipantsNotInQueue - ParticipantsInQueue).ToStringWithSuffix(" (осталось {0} место)", " (осталось {0} места)", " (осталось {0} мест)");
                StatusTextShort = StatusText;
                LabelBadgeType = LabelType.WarningOrange;
                AlertType = AlertType.WarningOrange;
                PanelType = PanelType.WarningOrange;
                return;
            }

            if (NotInQueueLimit.HasValue && NotInQueueLimitByDistances != null && ParticipantsNowInQueueByDistances != null
                && ParticipantsNowInQueueByDistances.Any(x => NotInQueueLimitByDistances[x.Key].HasValue && x.Value >= (0.95 * NotInQueueLimitByDistances[x.Key].Value)))
            {
                Status = MeetingStatusEnum.RegOpenNearLimit;
                StatusText += " (последние места)";
                StatusTextShort = StatusText;
                LabelBadgeType = LabelType.WarningOrange;
                AlertType = AlertType.WarningOrange;
                PanelType = PanelType.WarningOrange;
                return;
            }

            var leftToClose = RegCloseTime.Subtract(DateTimeOffset.Now);
            var hurryUp = leftToClose.TotalDays < 7;
            if (hurryUp)
            {
                StatusText += " (" + leftToClose.ToHumanIntervalLeft() + ")";
                StatusTextShort = StatusText;
                LabelBadgeType = LabelType.WarningOrange;
                AlertType = AlertType.WarningOrange;
                PanelType = PanelType.WarningOrange;
            }
        }

        public bool MeetingOwnerOrFriend(Guid userId)
        {
            return OwnerUserId == userId || FriendUserIds.Contains(userId);
        }

        public void IncrementUnaccountedParticipants()
        {
            Interlocked.Increment(ref unaccountedParticipantsValue);
        }

        public void ResetUnaccountedParticipants()
        {
            unaccountedParticipantsValue = 0;
        }
    }
}