﻿namespace System
{
    public static class Int32Extensions
    {
        public static string ToBeautyFilesize(this int value)
        {
            if (value > (1024 * 1024 * 2))
            {
                return (value / 1024 / 1024).ToString("#### MB");
            }

            if (value > (1024 * 5))
            {
                return (value / 1024).ToString("#### KB");
            }

            return value.ToString("#### B");
        }

        public static string ToStringWithSuffix(this int value, string for1, string for2, string for5)
        {
            var mod = value % 100;
            if (mod >= 11 && mod <= 20)
            {
                return string.Format(for5, value);
            }

            mod = value % 10;
            if (mod == 1)
            {
                return string.Format(for1, value);
            }

            if (mod >= 2 && mod <= 4)
            {
                return string.Format(for2, value);
            }

            return string.Format(for5, value);
        }
    }
}
