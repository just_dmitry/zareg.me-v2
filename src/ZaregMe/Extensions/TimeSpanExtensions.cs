﻿namespace System
{
    public static class TimeSpanExtensions
    {
        public static string ToHumanInterval(this TimeSpan value)
        {
            if (value.TotalMinutes < 5)
            {
                return "меньше пяти минут";
            }

            if (value.TotalMinutes < 90)
            {
                return ((int)value.TotalMinutes).ToStringWithSuffix("{0} минута", "{0} минуты", "{0} минут");
            }

            if (value.TotalHours < 30)
            {
                return ((int)value.TotalHours).ToStringWithSuffix("{0} час", "{0} часа", "{0} часов");
            }

            return ((int)value.TotalDays).ToStringWithSuffix("{0} день", "{0} дня", "{0} дней");
        }

        public static string ToHumanIntervalLeft(this TimeSpan value)
        {
            if (value.TotalMinutes < 5)
            {
                return "осталось меньше пяти минут";
            }

            if (value.TotalMinutes < 90)
            {
                return ((int)value.TotalMinutes).ToStringWithSuffix("осталась {0} минута", "осталось {0} минуты", "осталось {0} минут");
            }

            if (value.TotalHours < 30)
            {
                return ((int)value.TotalHours).ToStringWithSuffix("остался {0} час", "осталось {0} часа", "осталось {0} часов");
            }

            return ((int)value.TotalDays).ToStringWithSuffix("остался {0} день", "осталось {0} дня", "осталось {0} дней");
        }
    }
}