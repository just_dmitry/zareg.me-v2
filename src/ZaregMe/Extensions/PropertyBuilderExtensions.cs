﻿namespace Microsoft.EntityFrameworkCore
{
    using Metadata.Builders;

    public static class PropertyBuilderExtensions
    {
        public static PropertyBuilder<TProperty> DateTimeOffsetZeroPrecision<TProperty>(this PropertyBuilder<TProperty> propertyBuilder)
        {
            return propertyBuilder.ForSqlServerHasColumnType("datetimeoffset(0)");
        }

        public static PropertyBuilder<TProperty> TimeZeroPrecision<TProperty>(this PropertyBuilder<TProperty> propertyBuilder)
        {
            return propertyBuilder.ForSqlServerHasColumnType("time(0)");
        }
    }
}
