﻿namespace System
{
    public static class DateTimeOffsetExtensions
    {
        public static DateTimeOffset Min(this DateTimeOffset dummy, DateTimeOffset val1, DateTimeOffset val2)
        {
            return val1 < val2 ? val1 : val2;
        }
    }
}
