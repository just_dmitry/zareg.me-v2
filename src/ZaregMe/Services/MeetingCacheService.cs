﻿namespace ZaregMe.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using Model;
    using RecurrentTasks;
    using ViewModels;

    public class MeetingCacheService : TaskBase<TaskRunStatus>
    {
        private static readonly TimeSpan ReloadAllPeriod = TimeSpan.FromMinutes(20);

        private readonly object syncRoot = new object();

        private DateTimeOffset nextReloadAll = DateTimeOffset.MinValue;

        private Dictionary<int, MeetingInfo> allUnarchivedMeetings = new Dictionary<int, MeetingInfo>();

        private List<MeetingInfo> regIndexPageMeetings = new List<MeetingInfo>();

        private HashSet<int> archivedMeetings = new HashSet<int>();

        private HashSet<int> shutdownMeetings = new HashSet<int>();

        //// private Dictionary<int, Subdomain> allSubdomains = new Dictionary<int, Subdomain>();

        private List<Guid> adminUsers = new List<Guid>();

        private bool reloadAllRequired = false;

        private List<int> reloadQueue = new List<int>();

        public MeetingCacheService(ILoggerFactory loggerFactory, IServiceScopeFactory serviceScopeFactory)
            : base(loggerFactory, TimeSpan.FromMinutes(1), serviceScopeFactory)
        {
            // nothing
        }

        public List<MeetingInfo> GetIndexPageMeetings(int? subdomainId = default(int?))
        {
            if (subdomainId.HasValue)
            {
                return regIndexPageMeetings.Where(m => m.SubdomainId == subdomainId.Value).ToList();
            }
            else
            {
                return regIndexPageMeetings;
            }
        }

        public void ReloadMeeting(int id)
        {
            lock (syncRoot)
            {
                if (!reloadQueue.Contains(id))
                {
                    reloadQueue.Add(id);
                }
            }

            TryRunImmediately();
        }

        public void ReloadAllMeetings()
        {
            reloadAllRequired = true;
            TryRunImmediately();
        }

        public MeetingInfo Find(int id)
        {
            MeetingInfo mi;
            if (allUnarchivedMeetings.TryGetValue(id, out mi))
            {
                return mi;
            }

            return new MeetingInfo { Id = id };
        }

        public bool IsShutdown(int id)
        {
            return shutdownMeetings.Contains(id);
        }

        public bool IsArchived(int id)
        {
            return archivedMeetings.Contains(id);
        }

        protected override void Run(IServiceProvider serviceProvider, TaskRunStatus runStatus)
        {
            var db = serviceProvider.GetRequiredService<ZaregMeDb>();

            if (DateTimeOffset.Now > nextReloadAll)
            {
                Logger.LogDebug($"{nameof(nextReloadAll)} passed ({nextReloadAll}), forcing full reload");
                reloadAllRequired = true;
            }

            if (reloadAllRequired)
            {
                Logger.LogDebug($"'{nameof(reloadAllRequired)}' is true.");

                var now = DateTimeOffset.Now;

                allUnarchivedMeetings = db.Meetings
                    .Where(x => x.ArchiveTime > DateTimeOffset.Now)
                    .Select(x => new MeetingInfo()
                    {
                        Id = x.Id,
                        StartDate = x.StartDate,
                        Name = x.Name,
                        Location = x.Location,
                        SmallLogoName = x.SmallLogoName,
                        IsHiddenFromStartPage = x.IsHidden,
                        IsVerified = x.IsVerified,
                        SportType = x.SportType,
                        SubdomainId = x.SubdomainId,
                        TimeZoneOffset = x.TimeZoneOffset,
                        OwnerUserId = x.OwnerUserId,
                        ShutdownTime = x.ShutdownTime,
                        ArchiveTime = x.ArchiveTime
                    })
                    .ToDictionary(x => x.Id);
                regIndexPageMeetings = allUnarchivedMeetings.Values
                    .Where(x => x.StartDate >= now.Date.Date && !x.IsHiddenFromStartPage && x.IsVerified)
                    .OrderBy(x => x.StartDate)
                    .ThenBy(x => x.Name)
                    .ToList();

                var archived = db.Meetings.Where(m => m.ArchiveTime < now).Select(m => m.Id);
                archivedMeetings = new HashSet<int>(archived);

                var shutdown = db.Meetings.Where(m => m.ShutdownTime < now).Select(m => m.Id);
                shutdownMeetings = new HashSet<int>(shutdown);

                adminUsers = db.Users.Where(x => x.IsAdmin).Select(x => x.Id).ToList();

                lock (syncRoot)
                {
                    reloadQueue = allUnarchivedMeetings.Values
                        .Where(x => x.ShutdownTime > now && x.ArchiveTime > now)
                        .Select(x => x.Id)
                        .ToList();
                }

                nextReloadAll = now.Add(ReloadAllPeriod);
                reloadAllRequired = false;
                Logger.LogDebug($"ReloadAll done (next after {nextReloadAll}): {nameof(reloadQueue.Count)}={reloadQueue.Count}");
            }

            while (reloadQueue.Count > 0)
            {
                var id = reloadQueue[0];
                Logger.LogDebug($"Reloading info for meeting #{id}");

                ReloadOne(id, db);
                reloadQueue.RemoveAt(0);
            }
        }

        protected void ReloadOne(int id, ZaregMeDb db)
        {
            MeetingInfo mi;
            if (!allUnarchivedMeetings.TryGetValue(id, out mi))
            {
                Logger.LogWarning($"MeetingInfo not found for id={id}");
                return;
            }

            var meeting = db.Meetings
                .First(x => x.Id == id);

            mi.CopyFrom(meeting);
            mi.RefreshStatus();

            Logger.LogInformation($"{nameof(ReloadOne)}({id}) done. Status: {mi.Status}");
        }
    }
}
